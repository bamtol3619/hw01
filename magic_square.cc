//2015004057 김범수
#include <iostream>
#include <stdlib.h>

using namespace std;

int fill(int** p,int n);

int main(){
	int n,i,j;
	cin >> n;
	if(n>0 && n%2==1){
		int** p=(int**)malloc(sizeof(int*)*n);
		for(i=0;i<n;++i)	p[i]=(int*)malloc(sizeof(int)*n);
		fill(p,n);
		for(i=0;i<n;++i){
			for(j=0;j<n;++j)	cout << p[i][j] << " ";
			cout << endl;
		}
		free(p);
	}
	return 0;
}

int fill(int** p,int n){
	int row=0,col=(n/2),i;
	p[row][col]=1;
	for(i=2;i<=n*n;++i){
		if(row-1>=0 && col+1<n){	//오른쪽 위에가 마방진 안에 있는 경우에
			if(p[row-1][col+1]==0){	//그 곳이 비어있다면 그 곳에 숫자를 넣고
				--row;
				++col;
				p[row][col]=i;
			}
			else{	//그렇지 않으면 아래에 숫자를 넣는다
				++row;
				p[row][col]=i;
			}
		}
		else if(row-1==-1 && col+1==n){	//오른쪽 위에가 마방진 위와 오른족으로 벗어난 경우에
			++row;
			p[row][col]=i;
		}
		else if(row-1==-1 && col+1<n){	//오른쪽 위에가 마방진 위로 벗어난 경우에
			row=n-1;
			++col;
			p[row][col]=i;
		}
		else if(row-1>=0 && col+1==n){	//오른쪽 위에가 마방진 오른쪽으로 벗어난 경우에
			col=0;
			--row;
			p[row][col]=i;
		}
	}
	return 0;
}
