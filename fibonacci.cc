//2015004057 김범수
#include <iostream>

using namespace std;

int fib(int n);

int main(){
	int n,i;
	cin >> n;
	if(n>0){
		for(i=1;i<=n;++i){
			cout << fib(i) << " ";
		}
		cout << endl;
	}
	return 0;
}

int fib(int n){
	if(n==1 || n==2)	return 1;
	else	return fib(n-2)+fib(n-1);
}
