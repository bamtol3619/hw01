//2015004057 김범수
#include <iostream>
#include <stdlib.h>

using namespace std;

int sort(int* p,int n);

int main(){
	int n,i;
	cin >> n;
	if(n>0){
		int* p=(int*)malloc(sizeof(int)*n);
		for(i=0;i<n;++i)	cin >> p[i];
		sort(p,n);
		for(i=0;i<n;++i)	cout << p[i] << " ";
		cout << endl;
		free(p);
	}
	return 0;
}

int sort(int* p,int n){
	int i,j,temp;
	for(i=0;i<n-1;++i){
		for(j=i+1;j<n;++j){
			if(p[i]>p[j]){
				temp=p[i];
				p[i]=p[j];
				p[j]=temp;
			}
		}
	}
	return 0;
}
